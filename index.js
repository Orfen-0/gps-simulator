var express = require('express');
var delim = new Buffer('\n');
var app = express();
var bsplit = require('buffer-split')
var polyline = require('@mapbox/polyline');
var gps = require('gps-simulator/gps-simulator.js');
var bodyParser = require('body-parser');
var base64= require('base-64');
var utf8 = require('utf8');
var sleep = require('sleep');
var net = require('net');
var jsonParser = bodyParser.json();
var socket = net.Socket();
var log4js = require('log4js');
var log = log4js.getLogger();
var moment = require('moment');
var needle = require('needle');


var multiplier = 1;
if (process.argv.length >= 2) {
	if (process.argv[2] == 'dev') {
		app.set('hostname', (process.env.HOST ||
'192.168.2.48'));
	} else if (process.argv[2] == 'prod') {
		app.set('hostname', (process.env.HOST || '147.102.4.183'));
	} else {
		log.info('Usage node index.js {dev|prod}');
		process.exit(1);
	}
} else {
	log.info('Usage node index.js {dev|prod}');
	process.exit(1);
}

app.set('port', (process.env.PORT || 5000))
app.use(jsonParser);
socket.connect(20001, app.get('hostname'), function() {
});
socket.on('error', function(err) {

	log.error(err);
	log.error(arguments);
});

socket.on('connect', function() {
	data='{"authHash" : "GPSSimulator"}\n';
	var sent=socket.write(data,function(){
		log.info('GPS Simulator connected and subscribed on ' + app.get('hostname') + ':' + app.get('port'));
	});
});
var order_dict={};

var totaldata="";
var tempdata;
socket.on('data', function(data) {

	if(!data.includes("\n")){
		totaldata=totaldata+data;
		log.info('Order received.');
	}
	else{
		var arr=bsplit(data, delim);
		totaldata=totaldata+arr[0];
		var json=JSON.parse(totaldata.toString());
		var base64st=json.find(function(json){
			return json.key=="payload";
		})
		var bytes=base64.decode(base64st.val);
		var order = utf8.decode(bytes);


		parseOrder(order);

		totaldata=arr[1];
	}
});

function parseOrder(json){
	var order = JSON.parse(json);
	var id=order.OrdID;

	if(!order_dict[id]===null && !order_dict[id]===undefined) {
		if(order_dict[id].OrdRoutinStatus == 10 && order.OrdRoutingStatus == 20){
			order_dict[id] = order;
			startOrderSimulation(order_dict[id]);
		} else {
			order_dict[id] = order;
		}

		//else if(order_dict[id].OrdRoutingStatus==30){
		//	order_dict[id].gpsSimulator.stop(function(){});
		//}
	} else {
		order_dict[id] = order;
		if(order_dict[id].OrdRoutingStatus==20){
			log.info("Starting simulation for order " + id + "...");
			startOrderSimulation(order_dict[id]);
		}
	}
}

socket.on('end', function() {
	log.info("GOODBYE :( ");
});


var gpsarray=[];

// create application/x-www-form-urlencoded parser
function startOrderSimulation(order){

	try {
		order.gpspoints=polylineToGPS(polyline.decode(order.Polyline));
		var gpsSimulator = new gps.GpsSimulator(order.gpspoints, order.OrdID,1000,50);
		order.gpsSimulator=gpsSimulator;
		order_dict[order.OrdId]=order;
		gpsSimulator.start(function(position, beStopped, movableObject, currentRouteIndex) {
			log.info(movableObject);
			var str = "Route " + currentRouteIndex + ", speed " + movableObject.velocity * 6 + " km/h";
			log.info(str);

			if(order.lasttimestamp == null || order.lasttimestamp== undefined){
				order.lasttimestamp=new Date();
			var gps_sensor = {
				"order_id": order.OrdID,
				"truck_license_plate" : order.OrdVchPlateNr,
				"timestamp" : order.lasttimestamp,
				"latitude" : position.latitude,
				"longitude" : position.longitude
			};
		}
		else{
			order.lasttimestamp=new Date(order.lasttimestamp.getTime()+60000*multiplier);
			var gps_sensor = {
				"order_id": order.OrdID,
				"truck_license_plate" : order.OrdVchPlateNr,
				"timestamp" : order.lasttimestamp,
				"latitude" : position.latitude,
				"longitude" : position.longitude
			};
		}
			var jsonstring = JSON.stringify(gps_sensor);
			var utfstr = utf8.encode(jsonstring);
			var encstr = base64.encode(utfstr);

			var publication= {
				"publication": [
					{
						"key": "topic",
						"type": "string",
						"val": "truck_location"
					},
					{
						"key": "truck_location",
						"type": "string",
						"val": encodeURI(order.OrdVchPlateNr)
					},
					{
						"key": "payload",
						"type": "string",
						"val": encstr
					}
				]
			};
			var args = {
				data :publication,
				headers: { "Content-Type": "application/json" }

			};
			if (!beStopped) {
				publishGps(args);
			}

			var currentposition = {latitude:position.latitude,longitude:position.longitude};
			order.currentposition=currentposition;
			order.currline=currentRouteIndex;
			order_dict[order.OrdID]=order;
			if (beStopped == true) {
				publishOrder(order.OrdID);
			}

		});
	} catch (err) {
		log.error('Unable to start GPS simulator.');
		log.error(order);
		log.error(err);
	}
}




function publishGps(args){
console.log(args.data);
var options = {
  headers: { 'Content-Type': 'application/json' }
}
needle.post("http://" + app.get('hostname') + ":20000/publish", args.data, options ,function(error,response){
	console.log(response.statusCode + response.statusMessage);
})
  .on('readable', function() { /* eat your chunks */ })
  .on('done', function() {
    console.log('Ready-o, friend-o.');
  })
}

function publishOrder(order_id){
	console.log(order_id);
	var order=order_dict[order_id];
	order.OrdRoutingStatus=30;
	order.OrdStatus=30;
	order.OrdLastUpdateDateTime=new moment(order.lasttimestamp).format("YYYY-MM-DD HH:mm:ss");
	delete order.gpspoints;
	delete order.gpsSimulator;
	delete order.currentposition;
	delete order.currline;
	delete order.lasttimestamp;
	var ordjson = JSON.stringify(order);
	var utfstr = utf8.encode(ordjson);
	var encstr = base64.encode(utfstr);

	var publication= {
		"publication": [
			{
				"key": "topic",
				"type": "string",
				"val": "sarmed_order"
			},
			{
				"key": "sarmed_order",
				"type": "int",
				"val": order_id
			},
			{
				"key": "payload",
				"type": "string",
				"val": encstr
			}
		]
	};
	var options = {
	  headers: { 'Content-Type': 'application/json' }
	}
	//
	needle.post("http://" + app.get('hostname') + ":20000/publish", publication, options ,function(error,response){
		console.log(response.statusCode + response.statusMessage);
	})
	  .on('readable', function() { /* eat your chunks */ })
	  .on('done', function() {
	    console.log('Ready-o, friend-o.');
	  })
	}



function polylineToGPS(points_array){
	var route=[];
	for (var i = 0; i < points_array.length-1; i++) {
		if(i % 2===0 || i==points_array.length-2){
		var json={ idx: i,
			acceleration: 160,
			from_location: { latitude:points_array[i][0], longitude:points_array[i][1]},
			to_location: { latitude:points_array[i+1][0],longitude:points_array[i+1][1] } };
		route.push(json);
	}
	}

	route.pop();
	return route;
}



app.post('/disruption',function(req,res){
	log.info("received disruption");
	log.info(req.body);
	var body=req.body;
	order_dict[body.order_id].gpsSimulator.stop(function(){});
	res.send("Order "+body.order_id+" stopped");
});

app.post('/continue',function(req,res){
	log.info("received continue");
	var body=req.body;

	var order=order_dict[body.order_id];
	order.gpspoints=order.gpspoints.splice(order.currline+1,order.gpspoints.length);
	order.gpspoints[0].from_location.latitude=order.currentposition.latitude;
	order.gpspoints[0].from_location.longitude=order.currentposition.longitude;
	continueOrderSimulation(order);
	res.send("Order "+body.order_id+" resuming.");
});

app.get('/port', function() {
	log.info("GPS Simulator is running at "+app.getHostname()+":" + app.get('port'))
});

app.listen(5000);
